<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\SlackController;

class LunchbotSpeakCommand extends Command {

	protected $name = 'lunchbot:speak';

	protected $description = 'Give lunch for the day';

	public function fire()
	    {

	        $crawler = new SlackController;
	        return $crawler->speak();
	        // $this->call('App\Http\Controllers\CrawlController@index');

	    }

}