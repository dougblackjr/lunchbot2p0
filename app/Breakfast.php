<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* Breakfast Model
* NAME: Name of meal, is it healthy (true false)
*/

class Breakfast extends Model
{

	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $table = 'breakfasts';

	protected $fillable = ['id', 'name', 'healthy'];

	public function lunchdates() {

		return $this->belongsToMany('App\Lunchdate');

	}

}