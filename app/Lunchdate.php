<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* Lunch Model
* NAME: Name of meal, is it healthy (true false), category id
*/

class Lunchdate extends Model
{

	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $table = 'lunchdates';

	protected $fillable = ['id', 'date'];

	public function lunches() {

		return $this->belongsToMany('App\Lunch');

	}

	public function breakfasts() {

		return $this->belongsToMany('App\Breakfast');

	}

}