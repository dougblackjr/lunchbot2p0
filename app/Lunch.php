<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* Lunch Model
* NAME: Name of meal, is it healthy (true false), category id
*/

class Lunch extends Model
{

	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $table = 'lunchs';

	protected $fillable = ['id', 'name', 'healthy', 'category_id'];

	public function category() {

		return $this->belongsTo('App\Category');
		
	}

	public function lunchdates() {

		return $this->belongsToMany('App\Lunchdate');

	}

}