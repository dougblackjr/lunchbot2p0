<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lunch;
use App\Breakfast;
use App\Category;
use App\Lunchdate;
use DateTime;

class LunchdateController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lunchdates = Lunchdate::orderBy('id','DESC')
            ->with('breakfasts','lunches.category')
            ->paginate(50);
        return view('lunchdate.index',compact('lunchdates'))
            ->with('i', ($request->input('page', 1) - 1) * 50);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lunches = Lunch::with('category')->get();
        $lCreate = Lunch::where('category_id', 1)->orderBy('name')->pluck('name','id');
        $lSoup = Lunch::where('category_id', 3)->orderBy('name')->pluck('name','id');
        $lGrill = Lunch::where('category_id',2)->orderBy('name')->pluck('name','id');
        $lDeli = Lunch::where('category_id',4)->orderBy('name')->pluck('name','id');
        $breakfasts = Breakfast::orderBy('name')->pluck('name','id');
        $categories = Category::orderBy('name')->pluck('name','id');
        return view('lunchdate.create', compact('lunches','breakfasts','lCreate','lSoup','lGrill','lDeli', 'categories'));
    }

    public function week()
    {
        
        // Find next Monday and get dates
        $date = new DateTime();
        $date->modify('monday this week');
        $dates[] = $date->format('Y-m-d');
        for ($i=0; $i < 4; $i++) { 
            $tempDate = $date->modify('+1 day');
            $dates[] = $date->format('Y-m-d');
        }

        $lunches = Lunch::with('category')->get()->sortBy('name');
        $lCreate = Lunch::where('category_id', 1)->pluck('name','id')->sortBy(function($c) {
            return $c;
        });
        $lSoup = Lunch::where('category_id', 3)->pluck('name','id')->sortBy(function($c) {
            return $c;
        });
        $lGrill = Lunch::where('category_id',2)->where('healthy', FALSE)->pluck('name','id')->sortBy(function($c) {
            return $c;
        });
        $lGrillHealthy = Lunch::where('category_id',2)->where('healthy', TRUE)->pluck('name','id')->sortBy(function($c) {
            return $c;
        });
        $lDeli = Lunch::where('category_id',4)->where('healthy', FALSE)->pluck('name','id')->sortBy(function($c) {
            return $c;
        });
        $lDeliHealthy = Lunch::where('category_id',4)->where('healthy', TRUE)->pluck('name','id')->sortBy(function($c) {
            return $c;
        });
        $breakfasts = Breakfast::where('healthy', FALSE)->pluck('name','id')->sortBy(function($c) {
            return $c;
        });
        $breakfastsHealthy = Breakfast::where('healthy', TRUE)->pluck('name','id')->sortBy(function($c) {
            return $c;
        });
        return view('lunchdate.week', compact('dates','lunches','breakfasts','breakfastsHealthy','lCreate','lSoup','lGrill','lDeli', 'lGrillHealthy', 'lDeliHealthy'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $lunchdate = Lunchdate::create([
            'date' => $request->input('date'),
        ]);

        // Lunches
        if (!empty($request->input('lunches')))
        {
            $lunchdate->lunches()->sync($request->input('lunches'));
        } else {
            $lunchdate->lunches()->detach();
        }
        // Breakfasts
        if (!empty($request->input('breakfasts')))
        {
            $lunchdate->breakfasts()->sync($request->input('breakfasts'));
        } else {
            $lunchdate->breakfasts()->detach();
        }

        return redirect()->route('lunchdate.index')
            ->with('success','Lunchdate created successfully');
    }

    public function storeMany(Request $request)
    {

        $dates = $request->input('date');
        $bfasts = $request->input('breakfasts');
        $lunches = $request->input('lunches');

        $i = 0;

        foreach ($dates as $date) {

            $lunchdate = Lunchdate::create([
                'date' => $date,
            ]);

            // Lunches
            $lunchdate->lunches()->sync($lunches[$i]);
            $lunchdate->breakfasts()->sync($bfasts[$i]);

            $i++;
            
        }

        return redirect()->route('lunchdate.index')
            ->with('success','Lunchdate created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lunchdate = Lunchdate::with('breakfasts','lunches.category')->find($id);
        return view('lunchdate.show',compact('lunchdate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lunchdate = Lunchdate::find($id);
        $lCreate = Lunch::where('category_id', 1)->pluck('name','id');
        $lSoup = Lunch::where('category_id', 3)->pluck('name','id');
        $lGrill = Lunch::where('category_id',2)->pluck('name','id');
        $lDeli = Lunch::where('category_id',4)->pluck('name','id');
        $breakfasts = Breakfast::pluck('name','id');
        return view('lunchdate.edit',compact('lunchdate','breakfasts','lCreate','lSoup','lGrill','lDeli'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $lunchdate = Lunchdate::find($id);
        $lunchdate->date = $request->input('date');
        $lunchdate->save();
        
        // Breakfasts
        if (!empty($request->input('breakfasts')))
        {
        
            $lunchdate->breakfasts()->sync($request->input('breakfasts'));
        
        } else {
        
            $lunchdate->breakfasts()->detach();
        
        }

        // Lunches
        if (!empty($request->input('lunches')))
        {
            
            $lunchdate->lunches()->sync($request->input('lunches'));

        } else {
            
            $lunchdate->lunches()->detach();

        }

        return redirect()->route('lunchdate.index')
            ->with('success','Lunchdate updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lunchdate::find($id)->delete();
        return redirect()->route('lunchdate.index')
            ->with('success','Lunchdate deleted successfully');
    }
}