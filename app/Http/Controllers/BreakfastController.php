<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Breakfast;

class BreakfastController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breakfasts = Breakfast::orderBy('id','DESC')->paginate(50);
        return view('breakfast.index',compact('breakfasts'))
            ->with('i', ($request->input('page', 1) - 1) * 50);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('breakfast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Breakfast::create([
            'name' => $request->input('name'),
            'healthy' => (!empty($request->input('healthy')) ? true : false)
        ]);

        return redirect()->route('breakfast.index')
            ->with('success','Breakfast created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breakfast = Breakfast::find($id);
        return view('breakfast.show',compact('breakfast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breakfast = Breakfast::find($id);
        return view('breakfast.edit',compact('breakfast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        Breakfast::find($id)->update([
            'name' => $request->input('name'),
            'healthy' => (!empty($request->input('healthy')) ? true : false)      
        ]);
        return redirect()->route('breakfast.index')
            ->with('success','Breakfast updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Breakfast::find($id)->delete();
        return redirect()->route('breakfast.index')
            ->with('success','Breakfast deleted successfully');
    }
}