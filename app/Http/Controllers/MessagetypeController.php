<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Messagetype;

class MessagetypeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $messagetypes = messagetype::orderBy('id','DESC')->paginate(50);
        return view('messagetype.index',compact('messagetypes'))
            ->with('i', ($request->input('page', 1) - 1) * 50);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('messagetype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        messagetype::create([
            'name' => $request->input('name')
        ]);

        return redirect()->route('messagetype.index')
            ->with('success','messagetype created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $messagetype = messagetype::find($id);
        return view('messagetype.show',compact('messagetype'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $messagetype = messagetype::find($id);
        return view('messagetype.edit',compact('messagetype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        messagetype::find($id)->update([
            'name' => $request->input('name')
        ]);
        return redirect()->route('messagetype.index')
            ->with('success','messagetype updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        messagetype::find($id)->delete();
        return redirect()->route('messagetype.index')
            ->with('success','messagetype deleted successfully');
    }
}