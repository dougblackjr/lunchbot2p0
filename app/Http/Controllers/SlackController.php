<?php

namespace App\Http\Controllers;

use App\Conversations\ExampleConversation;
use Illuminate\Http\Request;
use DateTime;
use App\Breakfast;
use App\Lunch;
use App\Lunchdate;
use App\Message;
use Illuminate\Database\Eloquent;

class SlackController extends Controller
{

	public function listening(Request $request)
	{

		// THIS IS THE SLASH COMMAND PORTION OF LUNCHBOT
		$payload = array();
		$text = !empty($request->input('text')) ? $request->input('text') : 'today';

		$token = $request->input('token');

		// # Check the token and make sure the request is from our team 
		if($token != env('SLACK_TOKEN'))
		{

			$msg = "I have died. I have no regrets. (Slack Token issue)";

			die($msg);

			echo $msg;
		
		}

		$user = $request->input('user_name');
		// Let's stop people from hearing it a thousand times
		$payload['response_type'] = 'ephemeral';
		
		$payload['text'] = $this->hears($text, $user);

		// $ch = curl_init(env('SLACK_URL'));
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		// curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// $result = curl_exec($ch);
		// $curlResultInfo = curl_getinfo($ch);
		// $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		// if($errno = curl_errno($ch)) {
		// 	$error_message = curl_strerror($errno);
		// 	return "cURL error ({$errno}):\n {$error_message}";
		// }
		// curl_close($ch);

		// // return true;
		ignore_user_abort(true);
		set_time_limit(0);

		ob_start();
		// do initial processing here
		echo json_encode($payload); // send the response
		header('Connection: close');
		header("Content-Type: application/json");
		header('Content-Length: '.ob_get_length());
		ob_end_flush();
		if( ob_get_level() > 0 ) ob_flush();
		flush();

	}


	public function speak()
	{
		$payload = array();
		$payload['channel'] = env('SLACK_CHANNEL');
		$payload['text'] = $this->hears('today', NULL);
		$payload['response_type'] = 'in_channel';

		$ch = curl_init(env('SLACK_URL'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$curlResultInfo = curl_getinfo($ch);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if($errno = curl_errno($ch)) {
			$error_message = curl_strerror($errno);
			return "cURL error ({$errno}):\n {$error_message}";
		}
		curl_close($ch);

		// return true;
	}


	// PRIVATE FUNCTIONS

	private function hears($text, $user)
	{
		// Test one
		// return $this->today();

		// responses
		// today
		if (strpos($text, 'today') || $text == 'today') {
			return $this->today($user);
		}
		// tomorrow
		if (strpos($text, 'tomorrow') || $text == 'tomorrow') {
			return $this->tomorrow($user);
		}
		// otherwise...
		return $this->message(env('SNARK_MSG'));
	}

	private function today($user = NULL)
	{

		$today = new DateTime();

		$food = Lunchdate::with('lunches.category','breakfasts')
			->where('date','=',$today->format('Y-m-d'))
			->first();

		if(!empty($food)) {

			$food->orderBy('lunches.category');

			$response = '';
			
			// Add user hello
			$response .= !is_null($user) ? $this->message(env('WELCOME_MSG')).' '.$user.'! ' : '' ;

			$response .= 'Today\'s lunches are: ';

			foreach ($food->lunches as $lunch) {

				$response .= ucfirst($lunch->category->name).': '.$lunch->name;

				if($lunch->healthy) {
					$response .= " :apple:\n";
				} else {
					$response .= "\n";
				}

			}

			$response .= 'Breakfast is ';

			$bfcount = count($food->breakfasts);
			$i = 1;
			foreach ($food->breakfasts as $breakfast) {
				
				$response .= $breakfast->name;
				
				if ($i < $bfcount) {
					
					$response .= ', ';
					
					if ($i == ($bfcount - 1)) {
						$response .= 'and ';
					}
					
					$i++;
				}
			}

		} else {

			exit();
			die();

		}

		return $response;

	}

	private function tomorrow()
	{

		$today = new DateTime();

		$food = Lunchdate::with('lunches.category','breakfasts')
			->where('date','=',$today->modify('+1 day')->format('Y-m-d'))
			->first();

		if(!empty($food)) {

			$food->orderBy('lunches.category');

			$response = 'Tomorrow, we have: ';

			foreach ($food->lunches as $lunch) {

				$response .= ucfirst($lunch->category->name).': '.$lunch->name;

				if($lunch->healthy) {
					$response .= " :apple:\n";
				} else {
					$response .= "\n";
				}

			}

			$response .= 'Breakfast is ';

			$bfcount = count($food->breakfasts);
			$i = 1;
			foreach ($food->breakfasts as $breakfast) {
				
				$response .= $breakfast->name;
				
				if ($i < $bfcount) {
					
					$response .= ', ';
					
					if ($i == ($bfcount - 1)) {
						$response .= 'and ';
					}
					
					$i++;
				}
			}

		} else {

			$response = 'I have no information about tomorrow!';

		}

		return $response;

	}

	private function message($type)
	{
		// Get messages
		$message = Message::where('message_type_id','=',$type)
			->inRandomOrder()
			->first();
		
		// Return a random
		return $message->text;
	}

}