<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;
use App\Messagetype;

class MessageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $messages = Message::orderBy('id','DESC')
            ->with('message_type')
            ->paginate(50);
        return view('message.index',compact('messages'))
            ->with('i', ($request->input('page', 1) - 1) * 50);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $messagetypes = Messagetype::pluck('name','id');
        return view('message.create', compact('messagetypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Message::create([
            'text' => $request->input('text'),
            'message_type_id' => $request->input('message_type_id')
        ]);

        return redirect()->route('message.index')
            ->with('success','Message created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = Message::with('message_type')->find($id);
        return view('message.show',compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = Message::find($id);
        $messagetypes = Messagetype::pluck('name','id');
        return view('message.edit',compact('message','messagetypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        message::find($id)->update([
            'text' => $request->input('text'),
            'message_type_id' => $request->input('message_type_id')
        ]);
        return redirect()->route('message.index')
            ->with('success','message updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Message::find($id)->delete();
        return redirect()->route('message.index')
            ->with('success','message deleted successfully');
    }
}