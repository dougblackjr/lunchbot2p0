<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lunch;
use App\Category;

class LunchController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lunches = Lunch::orderBy('id','DESC')
            ->with('category')
            ->paginate(50);
        return view('lunch.index',compact('lunches'))
            ->with('i', ($request->input('page', 1) - 1) * 50);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name','id');
        return view('lunch.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Lunch::create([
            'name' => $request->input('name'),
            'healthy' => (!empty($request->input('healthy')) ? true : false),
            'category_id' => $request->input('category_id')
        ]);

        return redirect()->route('lunch.index')
            ->with('success','Lunch created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lunch = Lunch::with('category')->find($id);
        return view('lunch.show',compact('lunch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lunch = Lunch::find($id);
        $categories = Category::pluck('name','id');
        return view('lunch.edit',compact('lunch','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        Lunch::find($id)->update([
            'name' => $request->input('name'),
            'healthy' => (!empty($request->input('healthy')) ? true : false),
            'category_id' => $request->input('category_id')
        ]);
        return redirect()->route('lunch.index')
            ->with('success','Lunch updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lunch::find($id)->delete();
        return redirect()->route('lunch.index')
            ->with('success','Lunch deleted successfully');
    }
}