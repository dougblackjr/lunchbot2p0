<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Breakfast;
use App\Lunch;
use App\Lunchdate;
use App\Category;

class AjaxController extends Controller
{

	public function breakfaststore(Request $request)
	{

		Breakfast::create([
			'name' => $request->input('name'),
			'healthy' => (!empty($request->input('healthy')) ? true : false)
		]);
	
		return redirect()->action('LunchdateController@create');

	}

	public function lunchstore(Request $request)
	{
		Lunch::create([
			'name' => $request->input('name'),
			'healthy' => (!empty($request->input('healthy')) ? true : false),
			'category_id' => $request->input('category_id')
		]);

		return redirect()->action('LunchdateController@create');
	}

}