<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* Category Model (Categories)
* NAME: Name of category, is it lunch (true false)
*/

class Messagetype extends Model
{

	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $table = 'message_types';

	protected $fillable = ['id', 'name'];

	public function messages() {

		return $this->hasMany('App\Message');

	}

}