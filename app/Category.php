<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* Category Model (Categories)
* NAME: Name of category, is it lunch (true false)
*/

class Category extends Model
{

	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $table = 'categories';

	protected $fillable = ['id', 'name', 'lunch'];

	public function lunches() {

		return $this->hasMany('App\Lunch');

	}

}