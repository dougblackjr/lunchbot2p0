<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* Category Model (Categories)
* NAME: Name of category, is it lunch (true false)
*/

class Message extends Model
{

	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $table = 'messages';

	protected $fillable = ['id', 'text', 'message_type_id'];

	public function message_type() {

		return $this->belongsTo('App\Messagetype');

	}

}