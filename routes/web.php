<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['get', 'post'], '/botman', 'BotManController@handle');

Route::match(['get'], '/', 'PagesController@index');

// CRUD ROUTES
Route::resource('category', 'CategoryController');
Route::resource('breakfast', 'BreakfastController');
Route::resource('lunch', 'LunchController');
Route::resource('lunchdate', 'LunchdateController');
Route::get('week', 'LunchdateController@week');
Route::post('week', 'LunchdateController@storeMany');
Route::resource('messagetype', 'MessagetypeController');
Route::resource('message', 'MessageController');

// AJAX ROUTES
Route::post('/addbfast', 'AjaxController@breakfaststore');
Route::post('/addlunch', 'AjaxController@lunchstore');

// Lunchbot slack webhook
// Route::get('today', 'BotManController@today');
Route::post('/listening', 'SlackController@listening');