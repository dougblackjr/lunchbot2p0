<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLunch extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lunchs', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->boolean('healthy');
			$table->unsignedInteger('category_id')->nullable();
			$table->foreign('category_id')
				->references('id')
				->on('categories')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
    public function down()
    {
        Schema::dropIfExists('lunchs');
    }
}
