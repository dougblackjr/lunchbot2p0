<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lunch_lunchdate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lunch_id');
            $table->integer('lunchdate_id');
            $table->timestamps();
        });

        Schema::create('breakfast_lunchdate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('breakfast_id');
            $table->integer('lunchdate_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lunch_lunchdate');
        Schema::dropIfExists('breakfast_lunchdate');
    }
}
