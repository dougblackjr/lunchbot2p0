
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});

$(document).ready(function($) {
	var is_ajax_fire = 0;

	/* Create new Item */
	$(".crud-submit").click(function(e){
	    e.preventDefault();
	    var form_action = $("#breakfastModal").find("form").attr("action");
	    var title = $("#breakfastModal").find("input[name='breakfast-name']").val();
	    var description = $("#breakfastModal").find("textarea[name='breakfast-healthy']").val();

	    $.ajax({
	        dataType: 'json',
	        type:'POST',
	        url: form_action,
	        data:{title:title, description:description}
	    }).done(function(data){
	        getPageData();
	        $(".modal").modal('hide');
	        toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 5000});
	    });

	});	
});