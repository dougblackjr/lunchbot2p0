@extends('layouts.default')
 
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Messagetypes</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('messagetype.create') }}"> Create New Messagetype</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Name</th>
            <th width="280px">Action</th>
        </tr>
    @foreach ($messagetypes as $key => $item)
    <tr>
        <td>{{ $item->name }}</td>
        <td>
            <a class="btn btn-info" href="{{ route('messagetype.show',$item->id) }}">Show</a>
            <a class="btn btn-primary" href="{{ route('messagetype.edit',$item->id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['messagetype.destroy', $item->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>

    {!! $messagetypes->render() !!}

@endsection