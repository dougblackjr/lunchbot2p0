@extends('layouts.default')
 
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Breakfasts</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('breakfast.create') }}"> Create New Breakfast</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Name</th>
            <th>Healthy</th>
            <th width="280px">Action</th>
        </tr>
    @foreach ($breakfasts as $key => $item)
    <tr>
        <td>{{ $item->name }}</td>
        <td>{{ $item->healthy }}</td>
        <td>
            <a class="btn btn-info" href="{{ route('breakfast.show',$item->id) }}">Show</a>
            <a class="btn btn-primary" href="{{ route('breakfast.edit',$item->id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['breakfast.destroy', $item->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>

    {!! $breakfasts->render() !!}

@endsection