@extends('layouts.default')
 
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Show Breakfast</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('breakfast.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $breakfast->name }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Is It Healthy?</strong>
                {{ $breakfast->healthy }}
            </div>
        </div>

    </div>

@endsection