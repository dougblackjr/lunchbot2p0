@extends('layouts.default')

@section('content')

	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Create New Weekly Schedule</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-primary" href="{{ route('lunchdate.index') }}"> Back</a>
			</div>
		</div>
	</div>

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	{!! Form::open(array('url' => '/week','method'=>'POST')) !!}
	@for ($i = 0; $i < 5; $i++)
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="form-group">
					<strong>Date:</strong>
					{!! Form::date('date['.$i.']', $dates[$i], array('class' => 'form-control' )) !!}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="form-group">
					<strong>Breakfast - Regular:</strong>
					{!! Form::select('breakfasts['.$i.'][]', $breakfasts, null, array('class' => 'form-control')) !!}
				</div>
			</div>

			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="form-group">
					<strong>Breakfast - Healthy:</strong>
					{!! Form::select('breakfasts['.$i.'][]', $breakfastsHealthy, null, array('class' => 'form-control weekBreakfast')) !!}
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="form-group">
					<strong>Grill - Regular:</strong>
					{!! Form::select('lunches['.$i.'][]', $lGrill, null, array('class' => 'form-control')) !!}
				</div>
			</div>		<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="form-group">
					<strong>Grill - Healthy:</strong>
					{!! Form::select('lunches['.$i.'][]', $lGrillHealthy, null, array('class' => 'form-control weekGrill')) !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="form-group">
					<strong>Soup:</strong>
					{!! Form::select('lunches['.$i.'][]', $lSoup, null, array('class' => 'form-control')) !!}
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="form-group">
					<strong>Deli - Regular:</strong>
					{!! Form::select('lunches['.$i.'][]', $lDeli, null, array('class' => 'form-control')) !!}
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="form-group">
					<strong>Deli - Healthy:</strong>
					{!! Form::select('lunches['.$i.'][]', $lDeliHealthy, null, array('class' => 'form-control weekDeli')) !!}
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="form-group">
					<strong>Create:</strong>
					{!! Form::select('lunches['.$i.'][]', $lCreate, null, array('class' => 'form-control')) !!}
				</div>
			</div>
		</div>
		<hr />
	@endfor
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
	{!! Form::close() !!}

<script>
	$(document).ready(function($) {
		$('.weekBreakfast').on('change', function(event) {
			var val = $(this).val();
			$('.weekBreakfast').val(val);
		});
		$('.weekGrill').on('change', function(event) {
			var val = $(this).val();
			$('.weekGrill').val(val);
		});
		$('.weekDeli').on('change', function(event) {
			var val = $(this).val();
			$('.weekDeli').val(val);
		});
	});
</script>

@endsection