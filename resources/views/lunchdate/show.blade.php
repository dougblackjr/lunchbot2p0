@extends('layouts.default')
 
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Show Lunchdate</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('lunchdate.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Date:</strong>
                {{ $lunchdate->date }}
            </div>
            <div>
                @foreach ($lunchdate->breakfasts as $bfast)
                    <p><strong>Breakfast:</strong> {{ $bfast->name }}</p>
                @endforeach
                @foreach ($lunchdate->lunches as $lunch)
                    <p><strong>Lunch - {{$lunch->category->name}}</strong>: {{ $lunch->name }}</p>
                @endforeach
            </div>
        </div>
    </div>

@endsection