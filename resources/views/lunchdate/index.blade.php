@extends('layouts.default')
 
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Lunchdates</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-warning" href="/week"> Create New Week</a>
                <a class="btn btn-success" href="{{ route('lunchdate.create') }}"> Create New Lunchdate</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Date</th>
            <th width="280px">Action</th>
        </tr>
    @foreach ($lunchdates as $key => $item)
    <tr>
        <td>{{ $item->date }}</td>
        <td>
            <a class="btn btn-info" href="{{ route('lunchdate.show',$item->id) }}">Show</a>
            <a class="btn btn-primary" href="{{ route('lunchdate.edit',$item->id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['lunchdate.destroy', $item->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>

    {!! $lunchdates->render() !!}

@endsection