@extends('layouts.default')
 
@section('content')

	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Edit Lunchdate</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-primary" href="{{ route('lunchdate.index') }}"> Back</a>
			</div>
		</div>
	</div>

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	
	{!! Form::model($lunchdate, ['method' => 'PATCH','route' => ['lunchdate.update', $lunchdate->id]]) !!}
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="form-group">
					<strong>Date:</strong>
					{!! Form::date('date', $lunchdate->date, array('class' => 'form-control')) !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="form-group">
					<strong>Breakfast:</strong>
					{!! Form::select('breakfasts[]', $breakfasts, $lunchdate->breakfasts->pluck('id')->toArray(), array('class' => 'form-control', 'multiple' => true, 'id' => 'breakfast-multiselect')) !!}
				</div>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="form-group">
					<strong>Lunch - Create:</strong>
					{!! Form::select('lunches[]', $lCreate, $lunchdate->lunches->pluck('id')->toArray(), array('class' => 'form-control', 'multiple' => true, 'id' => 'create-multiselect')) !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="form-group">
					<strong>Lunch - Soup:</strong>
					{!! Form::select('lunches[]', $lSoup, $lunchdate->lunches->pluck('id')->toArray(), array('class' => 'form-control', 'multiple' => true, 'id' => 'soup-multiselect')) !!}
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="form-group">
					<strong>Lunch - Grill:</strong>
					{!! Form::select('lunches[]', $lGrill, $lunchdate->lunches->pluck('id')->toArray(), array('class' => 'form-control', 'multiple' => true, 'id' => 'grill-multiselect')) !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="form-group">
					<strong>Lunch - Deli:</strong>
					{!! Form::select('lunches[]', $lDeli, $lunchdate->lunches->pluck('id')->toArray(), array('class' => 'form-control', 'multiple' => true, 'id' => 'deli-multiselect')) !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</div>
	{!! Form::close() !!}

@endsection