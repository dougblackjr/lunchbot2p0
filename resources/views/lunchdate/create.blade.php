@extends('layouts.default')

@section('content')

	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Create New Lunchdate</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-primary" href="{{ route('lunchdate.index') }}"> Back</a>
			</div>
		</div>
	</div>

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	{!! Form::open(array('route' => 'lunchdate.store','method'=>'POST')) !!}
	{!! Form::token() !!}
	<div class="row">

		<div class="col-xs-6 col-sm-6 col-md-6">
			<div class="form-group">
				<strong>Date:</strong>
				{!! Form::date('date', \Carbon\Carbon::now(), array('class' => 'form-control')) !!}
			</div>
		</div>

		<div class="col-xs-6 col-sm-6 col-md-6">
		<!-- Button trigger modal -->
			<div class="form group">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#breakfastModal">
					Add Breakfast
				</button>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#lunchModal">
					Add Lunch
				</button>
			</div>
		</div>

	</div>

	<div class="row">

		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6">
					<div class="form-group">
						<strong>Breakfast:</strong>
						{!! Form::select('breakfasts[]', $breakfasts, null, array('class' => 'form-control', 'multiple' => true, 'id' => 'breakfast-multiselect')) !!}
					</div>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-6">
					<div class="form-group">
						<strong>Lunch - Create:</strong>
						{!! Form::select('lunches[]', $lCreate, null, array('class' => 'form-control', 'multiple' => true, 'id' => 'create-multiselect')) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6">
					<div class="form-group">
						<strong>Lunch - Soup:</strong>
						{!! Form::select('lunches[]', $lSoup, null, array('class' => 'form-control', 'multiple' => true, 'id' => 'soup-multiselect')) !!}
					</div>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-6">
					<div class="form-group">
						<strong>Lunch - Grill:</strong>
						{!! Form::select('lunches[]', $lGrill, null, array('class' => 'form-control', 'multiple' => true, 'id' => 'grill-multiselect')) !!}
					</div>
				</div>

			<div class="row">

				<div class="col-xs-6 col-sm-6 col-md-6">
					<div class="form-group">
						<strong>Lunch - Deli:</strong>
						{!! Form::select('lunches[]', $lDeli, null, array('class' => 'form-control', 'multiple' => true, 'id' => 'deli-multiselect')) !!}
					</div>
				</div>
			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">Submit</button>
		</div>

	</div>
	{!! Form::close() !!}

	<!-- BREAKFAST MODAL -->
	<div class="modal fade" id="breakfastModal" tabindex="-1" role="dialog" aria-labelledby="breakfastModal" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Add Breakfast</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			{!! Form::open(array('url' => '/addbfast','method'=>'POST')) !!}
				{!! Form::token() !!}
    			<div class="row">
        			<div class="col-xs-12 col-sm-12 col-md-12">
            			<div class="form-group">
                			<strong>Name:</strong>
                			{!! Form::text('name', null, array('placeholder' => 'Classic Eggs','class' => 'form-control', 'id' => 'breakfast-name')) !!}
            			</div>
        			</div>

        			<div class="col-xs-12 col-sm-12 col-md-12">
            			<div class="form-group">
                			<strong>Is It Healthy?</strong>
                			{!! Form::checkbox('healthy', null, array('class' => 'form-control', 'id' => 'breakfast-healthy')) !!}
            			</div>
        			</div>

			    </div>

		</div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary crud-submit">Submit</button>
			{!! Form::close() !!}
		  </div>
		</div>
	  </div>
	</div>

	<!-- LUNCH MODAL -->
	<div class="modal fade" id="lunchModal" tabindex="-1" role="dialog" aria-labelledby="lunchModal" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Add Lunch</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">

		    {!! Form::open(array('url' => '/addlunch','method'=>'POST')) !!}
		    {!! Form::open(array('route' => 'lunch.store','method'=>'POST')) !!}
		    {!! Form::token() !!}
		    <div class="row">

		        <div class="col-xs-12 col-sm-12 col-md-12">
		            <div class="form-group">
		                <strong>Name:</strong>
		                {!! Form::text('name', null, array('placeholder' => 'Classic Sandwich Thin','class' => 'form-control')) !!}
		            </div>
		        </div>

		        <div class="col-xs-12 col-sm-12 col-md-12">
		            <div class="form-group">
		                <strong>Category:</strong>
		                {!! Form::select('category_id', $categories , null, array('class' => 'form-control')) !!}
		            </div>
		        </div>

		        <div class="col-xs-12 col-sm-12 col-md-12">
		            <div class="form-group">
		                <strong>Is It Healthy?</strong>
		                {!! Form::checkbox('healthy', null, array('class' => 'form-control')) !!}
		            </div>
		        </div>

		    </div>

		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Submit</button>
			{!! Form::close() !!}
		  </div>
		</div>
	  </div>
	</div>
@endsection