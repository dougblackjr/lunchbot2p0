@extends('layouts.default')
 
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Messages</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('message.create') }}"> Create New Message</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>text</th>
            <th>Type</th>
            <th width="280px">Action</th>
        </tr>
    @foreach ($messages as $key => $item)
    <tr>
        <td>{{ $item->text }}</td>
        <td>{{ $item->message_type->name }}
        <td>
            <a class="btn btn-info" href="{{ route('message.show',$item->id) }}">Show</a>
            <a class="btn btn-primary" href="{{ route('message.edit',$item->id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['message.destroy', $item->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>

    {!! $messages->render() !!}

@endsection